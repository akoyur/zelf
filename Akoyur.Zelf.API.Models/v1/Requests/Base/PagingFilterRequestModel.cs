﻿using System.Collections.Generic;

namespace Akoyur.Zelf.API.Models.v1.Requests.Base
{
	/// <summary>
	/// Постраничный поиск
	/// </summary>
	public class PagingFilterRequestModel : ValidatableRequestModel, IPagingFilterRequest
	{
		/// <inheritdoc cref="IPagingFilterRequest.Paging"/>
		public PagingRequestModel Paging { get; set; }

		/// <inheritdoc cref="IPagingFilterRequest.Sort"/>
		public SortFieldModel[] Sort { get; set; }

		/// <inheritdoc cref="IPagingFilterRequest.Where"/>
		public WhereFieldModel[] Where { get; set; }

		/// <inheritdoc cref="ValidateModel.GetValidationErrors"/>
		public override Dictionary<string, string> GetValidationErrors()
		{
			return GetPagingRequestValidationErrors((IPagingFilterRequest)this);
		}
	}
}