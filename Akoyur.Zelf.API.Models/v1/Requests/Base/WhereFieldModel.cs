﻿using Akoyur.Zelf.Enumerations;

namespace Akoyur.Zelf.API.Models.v1.Requests.Base
{
	public class WhereFieldModel
	{
		/// <summary>
		/// Наименование поля
		/// </summary>
		public string FieldName { get; set; }

		/// <summary>
		/// Тип условного выражения
		/// </summary>
		public WhereEnum Type { get; set; }

		/// <summary>
		/// Значение поля
		/// </summary>
		public string FieldValue { get; set; }

		/// <summary>
		/// Значение поля (коллекция)
		/// </summary>
		public string[] FieldValues { get; set; }
	}
}