﻿using Akoyur.Zelf.Enumerations;

namespace Akoyur.Zelf.API.Models.v1.Requests.Base
{
	public class SortFieldModel
	{
		/// <summary>
		/// Тип сортировки
		/// </summary>
		public SortEnum Type { get; set; }

		/// <summary>
		/// Наименование поля
		/// </summary>
		public string FieldName { get; set; }
	}
}