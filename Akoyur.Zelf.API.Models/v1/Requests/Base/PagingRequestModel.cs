﻿namespace Akoyur.Zelf.API.Models.v1.Requests.Base
{
	/// <summary>
	/// Постраничная фильтрация
	/// </summary>
	public class PagingRequestModel
	{
		/// <summary>
		/// Количество элементов, которые необходимо пропустить
		/// </summary>
		public int Skip { get; set; }

		/// <summary>
		/// Количество элементов, которое необходимо вернуть
		/// </summary>
		public int Take { get; set; }

		/// <summary>
		/// ctor
		/// </summary>
		public PagingRequestModel()
		{

		}

		/// <summary>
		/// ctor
		/// </summary>
		public PagingRequestModel(int skip, int take)
		{
			Skip = skip;
			Take = take;
		}
	}
}