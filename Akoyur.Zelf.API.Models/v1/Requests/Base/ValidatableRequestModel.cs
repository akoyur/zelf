﻿using System.Collections.Generic;
using System.Linq;
using Akoyur.Zelf.Helpers;

namespace Akoyur.Zelf.API.Models.v1.Requests.Base
{
    /// <summary>
    /// Валидируемый объект
    /// </summary>
    public abstract class ValidatableRequestModel
    {
        /// <summary>
        /// Проверить, является ли объект валидным
        /// </summary>
        /// <returns>True, если объект валидный</returns>
        public bool IsValid(out Dictionary<string, string> errors)
        {
            errors = GetValidationErrors();
            return errors == null || !errors.Keys.Any();
        }

        /// <summary>
        /// Получить подробные ошибки валидации
        /// </summary>
        /// <returns>Словарь ошибок {ключевое слово, сообщение об ошибке}</returns>
        public abstract Dictionary<string, string> GetValidationErrors();

        public Dictionary<string, string> GetPagingRequestValidationErrors(IPagingFilterRequest request)
        {
            var dictionary = new Dictionary<string, string>();

            if (request == null)
            {
                dictionary.Add("", ValidationMessageHelper.GetRequestParametersNotDefined());
                return dictionary;
            }

            if (request.Paging == null)
            {
                dictionary.Add("paging", ValidationMessageHelper.GetValueIsIncorrectOrNotDefined("paging"));
                return dictionary;
            }

            if (request.Paging.Skip < 0)
            {
                dictionary.Add("paging.skip", ValidationMessageHelper.GetIntValueShouldBeGreaterOrEqualThan("paging.skip", 0));
            }

            if (request.Paging.Take <= 0)
            {
                dictionary.Add("paging.take", ValidationMessageHelper.GetIntValueShouldBeGreaterThan("paging.take", 0));
            }

            return dictionary;
        }
    }
}