﻿namespace Akoyur.Zelf.API.Models.v1.Requests.Base
{
	/// <summary>
	/// Интерфейс постраничной фильтра
	/// </summary>
	public interface IPagingFilterRequest
	{
		/// <summary>
		/// Постраничная фильтрация
		/// </summary>
		PagingRequestModel Paging { get; set; }

		/// <summary>
		/// Сортировка
		/// </summary>
		SortFieldModel[] Sort { get; set; }

		/// <summary>
		/// Условия выборки
		/// </summary>
		WhereFieldModel[] Where { get; set; }
	}
}