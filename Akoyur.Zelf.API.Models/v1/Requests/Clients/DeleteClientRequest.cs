﻿using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;
using Akoyur.Zelf.API.Models.v1.Requests.Base;
using Akoyur.Zelf.Helpers;

namespace Akoyur.Zelf.API.Models.v1.Requests.Clients
{
    /// <summary>
    /// Запрос "Удалить клиента"
    /// </summary>
    public class DeleteClientRequest : ValidatableRequestModel
    {
        /// <summary>
        /// Уникальный идентификатор клиента
        /// </summary>
        [JsonPropertyName("id")]
        public int Id { get; set; }
        
        public override Dictionary<string, string> GetValidationErrors()
        {
            var dictionary = new Dictionary<string, string>();

            if (Id <= 0)
                dictionary.Add("id", ValidationMessageHelper.GetValueIsIncorrectOrNotDefined("id"));
            
            return dictionary;
        }

        public DeleteClientRequest()
        {
            
        }

        public DeleteClientRequest(int id)
        {
            Id = id;
        }
    }
}