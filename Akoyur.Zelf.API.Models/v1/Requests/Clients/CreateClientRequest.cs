﻿using System.Collections.Generic;
using System.Text.Json.Serialization;
using Akoyur.Zelf.API.Models.v1.Requests.Base;
using Akoyur.Zelf.Helpers;

namespace Akoyur.Zelf.API.Models.v1.Requests.Clients
{
    /// <summary>
    /// Запрос "Зарегистрировать нового клиента"
    /// </summary>
    public class CreateClientRequest : ValidatableRequestModel
    {
        /// <summary>
        /// Имя клиента
        /// </summary>
        [JsonPropertyName("name")]
        public string Name { get; set; }
        
        public override Dictionary<string, string> GetValidationErrors()
        {
            var dictionary = new Dictionary<string, string>();

            if (string.IsNullOrWhiteSpace(Name))
            {
                dictionary.Add("name", ValidationMessageHelper.GetValueIsIncorrectOrNotDefined("name"));
            }
            else
            {
                if (!RegExHelper.VerifyClientName(Name))
                    dictionary.Add("name", "Имя должно состоять из букв и пробелов и быть не длиннее 64 символов.");
            }

            return dictionary;
        }
    }
}