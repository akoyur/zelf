﻿using Akoyur.Zelf.API.Models.v1.Requests.Base;

namespace Akoyur.Zelf.API.Models.v1.Requests.Clients
{
    /// <summary>
    /// Запрос "Получить список клиентов"
    /// </summary>
    public class GetClientsRequest : PagingFilterRequestModel
    {
        
    }
}