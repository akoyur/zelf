﻿using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;
using Akoyur.Zelf.API.Models.v1.Requests.Base;
using Akoyur.Zelf.Helpers;

namespace Akoyur.Zelf.API.Models.v1.Requests.Clients
{
    /// <summary>
    /// Запрос "Подписать одного клиента на другого"
    /// </summary>
    public class SubscribeClientRequest : ValidatableRequestModel
    {
        /// <summary>
        /// Уникальный идентификатор клиента
        /// </summary>
        [JsonPropertyName("clientId")]
        public int ClientId { get; set; }
        
        /// <summary>
        /// Уникальный идентификатор клиента-подписчика
        /// </summary>
        [JsonPropertyName("subscriberId")]
        public int SubscriberId { get; set; }
        
        public override Dictionary<string, string> GetValidationErrors()
        {
            var dictionary = new Dictionary<string, string>();

            if (ClientId <= 0)
                dictionary.Add("clientId", ValidationMessageHelper.GetValueIsIncorrectOrNotDefined("clientId"));

            if (SubscriberId <= 0)
                dictionary.Add("subscriberId", ValidationMessageHelper.GetValueIsIncorrectOrNotDefined("subscriberId"));


            if (ClientId > 0 && ClientId == SubscriberId)
                dictionary.Add("clientId", "Ошибка. Создавать подписку на самого себя запрещено.");
            
            return dictionary;
        }
    }
}