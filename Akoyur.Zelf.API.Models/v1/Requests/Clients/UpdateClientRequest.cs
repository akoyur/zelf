﻿using System.Collections.Generic;
using System.Text.Json.Serialization;
using Akoyur.Zelf.API.Models.v1.Requests.Base;
using Akoyur.Zelf.Helpers;

namespace Akoyur.Zelf.API.Models.v1.Requests.Clients
{
    /// <summary>
    /// Запрос "Обновить клиента"
    /// </summary>
    public class UpdateClientRequest : ValidatableRequestModel
    {
        /// <summary>
        /// Уникальный идентификатор клиента
        /// </summary>
        [JsonPropertyName("id")]
        public int Id { get; set; }
        
        /// <summary>
        /// Имя клиента
        /// </summary>
        [JsonPropertyName("name")]
        public string Name { get; set; }
        
        public override Dictionary<string, string> GetValidationErrors()
        {
            var dictionary = new Dictionary<string, string>();

            if (Id <= 0)
                dictionary.Add("id", ValidationMessageHelper.GetValueIsIncorrectOrNotDefined("id"));

            if (string.IsNullOrWhiteSpace(Name))
                dictionary.Add("name", ValidationMessageHelper.GetValueIsIncorrectOrNotDefined("name"));

            return dictionary;
        }
    }
}