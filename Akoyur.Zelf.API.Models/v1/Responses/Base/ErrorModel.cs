﻿using System.Collections.Generic;

namespace Akoyur.Zelf.API.Models.v1.Responses.Base
{
    /// <summary>
    /// Модель данных описания ошибки
    /// </summary>
    public class ErrorModel
    {
        /// <summary>
        /// Сообщение об ошибке
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Коллекция ошибок {наименование параметра, сообщение об ошибке}
        /// </summary>
        public Dictionary<string,string> Errors { get; set; }

        /// <summary>
        /// ctor
        /// </summary>
        public ErrorModel()
        {
            //
        }

        /// <summary>
        /// ctor
        /// </summary>
        public ErrorModel(string message, Dictionary<string, string> errors = null)
        {
            Message = message;
            Errors = errors;
        }

        /// <summary>
        /// ctor
        /// </summary>
        public ErrorModel(Dictionary<string, string> errors)
        {
            Errors = errors;
        }
    }
}