﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace Akoyur.Zelf.API.Models.v1.Responses.Base
{
    /// <summary>
    /// Базовая модель ответа API
    /// </summary>
    public class BaseResponse
    {
        /// <summary>
        /// Сообщение об ошибке
        /// </summary>
        [JsonPropertyName("error")]
        public ErrorModel Error { get; set; }

        /// <summary>
        /// ctor
        /// </summary>
        public BaseResponse()
        {

        }

        /// <summary>
        /// ctor
        /// </summary>
        public BaseResponse(string errorMessage)
        {
            SetError(errorMessage);
        }

        /// <summary>
        /// ctor
        /// </summary>
        public BaseResponse(Dictionary<string, string> errors)
        {
            Error = new ErrorModel(errors);
        }

        /// <summary>
        /// ctor
        /// </summary>
        public BaseResponse(ErrorModel error)
        {
            Error = error;
        }

        /// <summary>
        /// Установить ошибку
        /// </summary>
        public virtual void SetError(string errorMessage)
        {
            Error = new ErrorModel(errorMessage);
        }
    }

    /// <summary>
    /// Базовая модель ответа API
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class BaseResponse<T> : BaseResponse where T : class
    {
        /// <summary>
        /// Объект
        /// </summary>
        [JsonPropertyName("data")]
        public T Data { get; set; }

        /// <summary>
        /// ctor
        /// </summary>
        public BaseResponse() : base()
        {

        }

        /// <summary>
        /// ctor
        /// </summary>
        public BaseResponse(T data)
        {
            Data = data;
            Error = null;
        }

        /// <summary>
        /// ctor
        /// </summary>
        public BaseResponse(string errorMessage) : base(errorMessage)
        {
            Data = null;
        }


        /// <summary>
        /// ctor
        /// </summary>
        public BaseResponse(Dictionary<string, string> errors) : base(errors)
        {
            Data = null;
        }

        /// <summary>
        /// ctor
        /// </summary>
        public BaseResponse(ErrorModel error) : base(error)
        {
            Data = null;
        }
        public override void SetError(string errorMessage)
        {
            Data = null;
            Error = new ErrorModel(errorMessage);
        }
    }
}