﻿using System.Collections.Generic;

namespace Akoyur.Zelf.API.Models.v1.Responses.Base
{
    /// <summary>
    /// Постраничный результат
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class PagingResultModel<T>
    {
        /// <summary>
        /// Общее количество элементов
        /// </summary>
        public int Total { get; set; }
	
        /// <summary>
        /// Коллекция элементов
        /// </summary>
        public IList<T> Items { get; set; }

        /// <summary>
        /// ctor
        /// </summary>
        public PagingResultModel()
        {
			
        }

        /// <summary>
        /// ctor
        /// </summary>
        public PagingResultModel(IList<T> items, int total)
        {
            Items = items;
            Total = total;
        }
    }
}