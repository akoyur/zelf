﻿using System;
using System.Text.Json.Serialization;

namespace Akoyur.Zelf.API.Models.v1.Responses.Clients
{
    /// <summary>
    /// Запрос "Подписать одного клиента на другого"
    /// </summary>
    public class SubscribeClientResponse
    {
        /// <summary>
        /// Уникальный идентификатор подписки
        /// </summary>
        [JsonPropertyName("id")]
        public int Id { get; set; }
        
        /// <summary>
        /// Дата регистрации подписки
        /// </summary>
        [JsonPropertyName("createdAt")]
        public DateTime CreatedAt { get; set; }
    }
}