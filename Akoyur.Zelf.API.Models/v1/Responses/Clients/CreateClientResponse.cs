﻿using System;
using System.Text.Json.Serialization;

namespace Akoyur.Zelf.API.Models.v1.Responses.Clients
{
    /// <summary>
    /// Ответ "Зарегистрировать нового клиента"
    /// </summary>
    public class CreateClientResponse
    {
        /// <summary>
        /// Уникальный идентификатор клиента
        /// </summary>
        [JsonPropertyName("id")]
        public int Id { get; set; }
        
        /// <summary>
        /// Имя клиента
        /// </summary>
        [JsonPropertyName("name")]
        public string Name { get; set; }
        
        /// <summary>
        /// Дата регистрации клиента
        /// </summary>
        [JsonPropertyName("createdAt")]
        public DateTime CreatedAt { get; set; }
        
        /// <summary>
        /// Количество клиентов, на которых данный клиент подписан
        /// </summary>
        [JsonPropertyName("subscriptionsCount")]
        public int SubscriptionsCount { get; set; }
        
        /// <summary>
        /// Количество клиентов, подписанных на данного клиента
        /// </summary>
        [JsonPropertyName("subscribersCount")]
        public int SubscribersCount { get; set; }
    }
}