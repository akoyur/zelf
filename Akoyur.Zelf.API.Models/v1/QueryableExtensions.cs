﻿using System;
using System.Linq;
using System.Linq.Expressions;
using Akoyur.Zelf.API.Models.v1.Requests.Base;
using Akoyur.Zelf.API.Models.v1.Responses.Base;
using Akoyur.Zelf.Enumerations;

namespace Akoyur.Zelf.API.Models.v1
{
	public static class QueryableExtensions
	{
		public static IQueryable<TEntity> WhereStringField<TEntity>(
			this IQueryable<TEntity> query,
			Func<IQueryable<TEntity>, string, IQueryable<TEntity>> eqPredicate,
			Func<IQueryable<TEntity>, string, IQueryable<TEntity>> ctPredicate,
			WhereFieldModel whereField,
			BaseResponse response
		)
		{
			if (whereField == null)
			{
				response.SetError("");
				return null;
			}

			if (string.IsNullOrWhiteSpace(whereField.FieldValue))
			{
				response.SetError($"Некорректное значение фильтрации {whereField.FieldName}.");
				return null;
			}

			if (whereField.Type == WhereEnum.CT)
			{
				if (ctPredicate != null)
					return ctPredicate(query, whereField.FieldValue);
			}

			if (whereField.Type == WhereEnum.EQ)
			{
				if (eqPredicate != null)
					return eqPredicate(query, whereField.FieldValue);
			}

			response.SetError($"Некорректное значение фильтрации {whereField.FieldName}[Type].");
			return null;
		}
	}
}