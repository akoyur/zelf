import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { ConfirmationDialogService } from './common/confirmation-dialog/confirmation-dialog.service';
import { AppComponent } from './app.component';
import { ConfirmationDialogComponent } from './common/confirmation-dialog/confirmation-dialog.component';

// Zelf-Manager / Clients
import { ClientAddComponent } from './zelf/clients/client-add.component';
import { ClientEditComponent } from './zelf/clients/client-edit.component';
import { ClientFormComponent } from './zelf/clients/client-form.component';
import { ClientListComponent } from './zelf/clients/client-list.component';

// Services
import { ClientsService } from './zelf/services/clients.service';

import { DemoMaterialModule } from './material-module';

@NgModule({
  declarations: [
    AppComponent,
    ConfirmationDialogComponent,

    // Zelf-Manager / Clients
    ClientAddComponent,
    ClientEditComponent,
    ClientFormComponent,
    ClientListComponent
  ],
  imports: [
    DemoMaterialModule,
    BrowserAnimationsModule,
    BrowserModule.withServerTransition({appId: 'ng-cli-universal'}),
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot([
      {path: '', component: ClientListComponent, pathMatch: 'full'},
      // Zelf-Manager / Clients
      {path: 'client-list', component: ClientListComponent},
      {path: 'client-add', component: ClientAddComponent},
      {path: 'client-edit/:id', component: ClientEditComponent},
    ], {relativeLinkResolution: 'legacy'})
  ],
  providers: [
    // API Services:
    ClientsService,
    // Libs:
    ConfirmationDialogService
  ],
  bootstrap: [AppComponent]
})

export class AppModule { }
