import {Client} from '../models/client';
import {Component, OnInit} from '@angular/core';
import {PageEvent} from "@angular/material/paginator";
import {ClientsService, GetClientsResponse, Response} from '../services/clients.service';
import {
  SearchFilter,
  SearchFilterPaging,
  SearchFilterSortField,
  SearchFilterWhereField,
  SortEnum,
  WhereEnum
} from '../models/searchFilter';
import {HttpErrorResponse} from "@angular/common/http";

@Component({
  templateUrl: './client-list.component.html'
})

export class ClientListComponent implements OnInit {

  skip: number = 0;
  take: number = 12;

  sortBy: string = "CreatedAt";
  sortType: string = "Desc";

  paginatorLength: number;
  paginatorPageIndex: number;
  paginatorPageSize: number;

  isLoading: boolean;
  errorMessage: string;
  filteredByClient: Client;
  clients: Client[];
  clientsTotal: number;
  pageEvent: PageEvent;

  constructor(private clientsService: ClientsService) {
  }

  ngOnInit() {
    this.skip = 0;
    this.paginatorPageIndex = 0;
    this.paginatorPageSize = this.take;

    this.loadClients();
  }

  onPaginatorEvent(event?: PageEvent) {
    this.skip = this.take * event.pageIndex;
    this.loadClients();
    return event;
  }

  loadClients() {
    this.setFlags(true);

    let paging = new SearchFilterPaging(this.skip, this.take);
    let sort = []; // SearchFilterSortField
    let where = []; // SearchFilterWhereField

    if (this.sortBy && this.sortType){
      sort.push(new SearchFilterSortField(this.sortType === "Asc" ? SortEnum.Asc : SortEnum.Desc, this.sortBy));
    }

    if (this.filteredByClient){
      where.push(new SearchFilterWhereField(WhereEnum.EQ, "LinksOfClientId", this.filteredByClient.id.toString()));
    }

    let filter = new SearchFilter(paging, sort, where);
    this.paginatorPageIndex = this.skip / this.take;

    this.clientsService.getClients(filter)
      .subscribe(
        (response: Response<GetClientsResponse>) => {
          if (response && response.data) {
            this.clients = response.data.items;
            this.clientsTotal = response.data.total;
          } else {
            this.clients = [];
            this.clientsTotal = 0;
          }
          this.paginatorLength = this.clientsTotal;
          this.setFlags(false);
        },
        (response: HttpErrorResponse) => {
          this.processRequestError(response);
        });
  }

  getUserFriendlyTime(datetime: string) {
    let d = new Date();
    let utcd = new Date(d.getUTCFullYear(),d.getUTCMonth(),d.getUTCDate(),d.getUTCHours(),d.getUTCMinutes(),d.getUTCSeconds());
    let time = utcd.getTime() - new Date(datetime).getTime();

    let days = Math.round(time / (1000 * 3600 * 24)); //Diference in Days
    if (days > 0)
      return days + " дн. назад"

    let hours = Math.round(time / (1000 * 3600)); //Diference in Hours
    if (hours > 0)
      return hours + " ч. назад";

    let minutes = Math.round(time / (1000 * 60)); //Diference in Minutes
    if (minutes > 0)
      return minutes + " мин. назад";

    let seconds = Math.round(time / (1000)); //Diference in Minutes
    return (seconds < 0 ? 0 : seconds ) + " сек. назад";
  }

  onSortChange(event){
    this.paginatorPageIndex = 0;
    this.paginatorPageSize = this.take;
    this.loadClients();
  }

  setFlags(isLoading: boolean){
    this.isLoading = isLoading;

    if (isLoading) {
      this.errorMessage = null;
    }
  }

  processRequestError(response: HttpErrorResponse){
    let { error } = response;
    if (!error || !error.error || !error.error.message){
      this.errorMessage = null;
    } else {
      this.errorMessage = error.error.message;
    }
    this.setFlags(false);
  }

  manageSubscription(client){
    this.filteredByClient = client;
    this.ngOnInit(); // Сбросить поисковые фильтры
  }

  cancelManageSubscription(){
    this.filteredByClient = null;
    this.ngOnInit(); // Сбросить поисковые фильтры
  }

  onSubscribe(clientId, subscriberId){
    this.clientsService.subscribeClient(clientId, subscriberId)
      .subscribe(
        (response: any) => {
          if (this.filteredByClient){
            this.filteredByClient.subscriptionsCount++;
          }
          this.loadClients();
        },
        (response: HttpErrorResponse) => {
          this.processRequestError(response);
        });
  }

  onUnSubscribe(clientId, subscriberId) {
      this.clientsService.unSubscribeClient(clientId, subscriberId)
        .subscribe(
          (response: any) => {
            if (this.filteredByClient){
              this.filteredByClient.subscriptionsCount--;
            }
            this.loadClients();
          },
          (response: HttpErrorResponse) => {
            this.processRequestError(response);
          });
    }
}
