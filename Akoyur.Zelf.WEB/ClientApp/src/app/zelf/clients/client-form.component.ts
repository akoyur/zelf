import { Client } from '../models/client';
import { Component, Input } from '@angular/core';
import {NUMPAD_NINE} from "@angular/cdk/keycodes";

@Component({
  selector: "client-form",
  templateUrl: './client-form.component.html'
})

export class ClientFormComponent {
  @Input() client: Client;
  @Input() errors: any;
  @Input() isSubmitting: boolean;

  keypress(event: any) {
    const pattern = /^[a-zA-Zа-яА-Я\s]*$/;
    let inputChar = event.key;
    if (!pattern.test(inputChar)) {
      event.preventDefault();
    }
    if (this.client.name && this.client.name.length > 64){
      event.preventDefault();
    }
  }
}
