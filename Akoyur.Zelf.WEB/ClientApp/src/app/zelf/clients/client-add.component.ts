import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Client } from '../models/client';
import { HttpErrorResponse } from '@angular/common/http';
import { ClientsService } from '../services/clients.service';

@Component({
  templateUrl: './client-add.component.html'
})

export class ClientAddComponent {

  errors: any = {}; // объект-контейнер ошибок
  onSubmitError: string = ""; // Общая ошибка сохранения
  isSubmitting: boolean = false; // Признак выполнения запроса сохранения
  client: Client = new Client(); // Новый клиент

  constructor(private clientsService: ClientsService, private router: Router) {
  }

  addClient() {
    let request = {
      name: this.client.name
    };
    this.setFlags(true);
    this.clientsService.addClient(request)
      .subscribe(
        data => {
          this.setFlags(false);
          return this.router.navigateByUrl("/client-list");
        },
        (response: HttpErrorResponse) => {
          this.processRequestError(response);
        });
  }

  setFlags(isSubmitting: boolean){
    this.isSubmitting = isSubmitting;
    if (isSubmitting) {
      this.errors = null;
      this.onSubmitError = null;
    }
  }

  processRequestError(response: HttpErrorResponse){
    let { error } = response;
    if (!error || !error.error || (!error.error.message && !error.error.errors)){
      this.errors = null;
      this.onSubmitError = "Произошла ошибка. Пожалуйста, попробуйте еще раз.";
    } else {
      this.errors = error.error.errors;
      this.onSubmitError = error.error.message;
    }
    this.setFlags(false);
  }
}
