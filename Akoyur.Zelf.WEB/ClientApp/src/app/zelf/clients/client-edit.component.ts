import { Client } from '../models/client';
import { Component } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { Response, ClientsService, GetClientResponse } from '../services/clients.service';
import { ConfirmationDialogService } from '../../common/confirmation-dialog/confirmation-dialog.service';

@Component({
  templateUrl: './client-edit.component.html'
})

export class ClientEditComponent {

  id: number;
  errors: any = {};
  onOnInitError: string = "";
  onSubmitError: string = "";
  isLoading: boolean = true;
  isSubmitting: boolean = false;
  isDeleting: boolean = false;
  client: Client = new Client(); // Клиент

  constructor(
    private clientsService: ClientsService,
    private confirmationDialogService: ConfirmationDialogService,
    private router: Router, activeRoute: ActivatedRoute
  ) {
    this.id = activeRoute.snapshot.params["id"];
  }

  ngOnInit() {
    if (!this.id) {
      this.onOnInitError = "Не задан идентификатор клиента";
      return;
    }
    this.loadClient();
  }

  loadClient(){
    if (!this.id)
      return;

    this.clientsService.getClient(this.id)
      .subscribe(
        (response: Response<GetClientResponse>) => {
          if (response && response.data) {
            this.client = response.data;
            this.onOnInitError = null;
          } else {
            this.client = null;
            this.onOnInitError = "Клиент не найден";
          }
          this.isLoading = false;
        },
        (response: HttpErrorResponse) => {
          this.processRequestError(response);
        });
  }

  updateMerchant() {
    let request = {
      id: this.client.id,
      name: this.client.name
    };

    this.setFlags(true, false);

    this.clientsService.updateClient(this.client.id, request)
      .subscribe(
        data => {
          this.setFlags(false, false);
          return this.router.navigateByUrl("/client-list");
        },
        (response: HttpErrorResponse) => {
          this.processRequestError(response);
        });
  }

  deleteClient() {
    this.confirmationDialogService.confirm(
      'Подтверждение действия',
      'Удалить клиента?',
      'Да','Отмена','lg')
      .then((confirmed) => {
        this.setFlags(false, true);

        this.clientsService.deleteClient(this.client.id)
          .subscribe(
            data => {
              this.client.isDeleted = true;
              this.setFlags(false, false);
              return this.router.navigateByUrl("/client-list");
            },
            (response: HttpErrorResponse) => {
              this.processRequestError(response);
            });
      });
  }

  setFlags(isSubmitting: boolean, isDeleting: boolean,){
    this.isSubmitting = isSubmitting;
    this.isDeleting = isDeleting;

    if (isSubmitting || isDeleting) {
      this.errors = null;
      this.onSubmitError = null;
    }
  }

  processRequestError(response: HttpErrorResponse){
    let { error } = response;
    if (!error || !error.error || (!error.error.message && !error.error.errors)){
      this.errors = null;
      this.onSubmitError = "Произошла ошибка. Пожалуйста, попробуйте еще раз.";
    } else {
      this.errors = error.error.errors;
      this.onSubmitError = error.error.message;
    }
    this.setFlags(false, false);
  }
}
