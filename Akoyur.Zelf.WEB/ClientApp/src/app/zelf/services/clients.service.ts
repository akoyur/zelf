﻿import { Inject, Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Client } from '../models/client';
import { SearchFilter } from '../models/searchFilter';

@Injectable() export class ClientsService {

  private url = "/clients";

  constructor(private http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    this.url = baseUrl + this.url;
  }

  subscribeClient(clientId: number, subscriberId: number) {
    let request = { ClientId: clientId,  SubscriberId: subscriberId };
    return this.http.post(this.url + '/'+ clientId + '/subscriptions', request);
  }

  unSubscribeClient(clientId: number, subscriptionId: number) {
    return this.http.delete(this.url + '/' + clientId + '/subscriptions/' + subscriptionId);
  }

  addClient(request: any) {
    return this.http.post(this.url, request);
  }

  updateClient(clientId: number, request: any) {
    return this.http.put(this.url + '/'+ clientId, request);
  }

  deleteClient(clientId: number) {
    return this.http.delete(this.url +'/'+ clientId);
  }

  getClient(clientId: number) {
    return this.http.get(this.url +'/'+ clientId);
  }

  getClients(searchFilter: SearchFilter) {
    return this.http.post<Response<GetClientsResponse>>(this.url + '/search', searchFilter);
  }
}

export class Response<T> {
  constructor(
    public data?: T
  ) {  }
}

export class GetClientsResponse {
  constructor(
    public items?: Client[], // Коллекция элементов
    public total?: number, // Общее количество элементов
  ) {  }
}

export class GetClientResponse {
  constructor(
    public id?: number, // Идентификатор клиента
    public name?: string, // Имя клиента
    public createdAt?: string, // Дата регистрации клиента
    public updatedAt?: string, // Дата последнего обновления клиента
  ) {
  }
}
