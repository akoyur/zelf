﻿// Фильтр источников данных
export class SearchFilter {
  constructor(
    public paging? : SearchFilterPaging,
    public sort? : SearchFilterSortField[],
    public where? : SearchFilterWhereField[]
  ) {  }
}

export class SearchFilterPaging {
  constructor(
    public skip? : number,
    public take? : number
  ) {  }
}

export class SearchFilterSortField {
  constructor(
    public type? : SortEnum,
    public fieldName? : string
  ) {  }
}

export class SearchFilterWhereField {
  constructor(
    public type? : WhereEnum,
    public fieldName? : string,
    public fieldValue? : string,
    public fieldValues? : string[]
  ) {  }
}

// Тип источника данных
export enum SortEnum {
  Asc = "Asc", // Ascending
  Desc = "Desc" // Descending
}

// Тип источника данных
export enum WhereEnum {
  EQ = 1, // Equal
  LT = 2, // Less than
  LTOQ = 3, // Less than or equal
  GT = 4, // Greater than
  GTOQ = 5, // Greater than or equal
  CT = 6, // Contains
  AEQ = 7 // Any Equal
}
