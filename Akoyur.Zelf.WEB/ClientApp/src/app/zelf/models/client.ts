﻿// Клиент
export class Client {
  constructor(
    public id?: number, // Идентификатор клиента
    public name?: string, // Имя клиента
    public createdAt?: string, // Дата регистрации клиента
    public updatedAt?: string, // Дата последнего обновления клиента
    public isDeleted?: boolean, // Признак, что клиент удален
    public deletedAt?: string, // Дата удаления клиента
    public subscribersCount?: number, // Количество подписчиков
    public subscriptionsCount?: number, // Количество подписок
  ) {
  }
}
