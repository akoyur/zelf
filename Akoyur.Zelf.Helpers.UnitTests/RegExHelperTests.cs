using NUnit.Framework;
using Shouldly;

namespace Akoyur.Zelf.Helpers.UnitTests
{
    public class RegExHelperTests
    {
        [TestCase("Олег")]
        [TestCase("Олег Валерьевич")]
        [TestCase("АААААААААААААААААААААААААААААААААААААААААААААААААААААААААААААААА")] // 64 символа
        public void VerifyClientName_Valid_Name(string name)
        {
            // Act
            var result = RegExHelper.VerifyClientName(name);
            
            // Assert
            result.ShouldBeTrue();
        }
        
        [TestCase("Олег #")] // символ
        [TestCase("Олег Валерьевич 1")] // цифра
        [TestCase("ААААААААААААААААААААААААААААААААААААААААААААААААААААААААААААААААА")] // 65 символов
        public void VerifyClientName_Incorrect_Name(string name)
        {
            // Act
            var result = RegExHelper.VerifyClientName(name);
            
            // Assert
            result.ShouldBeFalse();
        }
    }
}