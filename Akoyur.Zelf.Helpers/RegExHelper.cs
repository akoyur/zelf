﻿using System.Text.RegularExpressions;

namespace Akoyur.Zelf.Helpers
{
    /// <summary>
    /// Класс-хелпер (Валидация регулярными выражениями)
    /// </summary>
    public static class RegExHelper
    {
        private static readonly Regex ClientNameRegEx = new Regex(@"^[a-zA-Zа-яА-ЯёЁ\s]{1,64}$");

        /// <summary>
        /// Валидация параметра "Имя клиента"
        /// </summary>
        /// <param name="name">Имя клиента</param>
        /// <returns>True/False</returns>
        public static bool VerifyClientName(string name)
        {
            return ClientNameRegEx.IsMatch(name);
        }
    }
}