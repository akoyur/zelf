﻿using System;

namespace Akoyur.Zelf.Helpers
{
    /// <summary>
    /// Хелпер генерации сообщений об ошибках валидации данных
    /// </summary>
    public static class ValidationMessageHelper
    {
        private const string RequestParametersNotDefined = "Не заданы параметры запроса";
        private const string ValueIsIncorrectOrNotDefined = "Значение {0} не задано или некорректно";
        private const string IntValueShouldBeGreaterOrEqualThan = "Значение {0} должно быть целым числом большим или равным {1}";
        private const string IntValueShouldBeGreaterThan = "Значение {0} должно быть целым числом большим {1}";

        /// <summary>
        /// Значение {0} не задано или некорректно
        /// </summary>
        /// <param name="name">Наименование параметра</param>
        public static string GetValueIsIncorrectOrNotDefined(string name) => string.Format(ValueIsIncorrectOrNotDefined, name);

        /// <summary>
        /// Значение {0} должно быть целым числом большим {1}
        /// </summary>
        /// <param name="name">Наименование параметра</param>
        public static string GetIntValueShouldBeGreaterThan(string name, int value) => string.Format(IntValueShouldBeGreaterThan, name, value);

        /// <summary>
        /// Значение {0} должно быть целым числом большим или равным {1}
        /// </summary>
        /// <param name="name">Наименование параметра</param>
        public static string GetIntValueShouldBeGreaterOrEqualThan(string name, int value) => string.Format(IntValueShouldBeGreaterOrEqualThan, name, value);

        /// <summary>
        /// Не заданы параметры запроса
        /// </summary>
        public static string GetRequestParametersNotDefined() => RequestParametersNotDefined;
    }
}