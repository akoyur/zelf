using System;
using System.Collections.Generic;
using System.Threading;
using Akoyur.Zelf.API.Models.v1.Requests.Clients;
using Akoyur.Zelf.Models.DbModels;
using Akoyur.Zelf.Storage;
using Moq;
using NUnit.Framework;
using Shouldly;

namespace Akoyur.Zelf.Core.UnitTests.ClientsApi
{
    public class CreateClientTests
    {
        [Test]
        public void CreateClient_Success()
        {
            // Arrange
            var randomId = new Random().Next(0, 10000);
            var clients = new List<Client>();
            
            var context = new Mock<ZelfContext>();
            context.Setup(x => x.Clients.AddAsync(It.IsAny<Client>(), It.IsAny<CancellationToken>()))
                .Callback((Client model, CancellationToken token) =>
                {
                    model.Id = randomId;
                    clients.Add(model);
                });
                        
            var contextFactory = new Mock<IContextFactory>();
            contextFactory
                .Setup(x => x.Create())
                .Returns(context.Object);

            var api = new Core.ClientsApi(contextFactory.Object);

            var request = new CreateClientRequest
            {
                Name = "Аркадий Петрович"
            };

            // Act
            var response = api.CreateClientAsync(request);

            // Assert
            response.ShouldSatisfyAllConditions(
                () => response.ShouldNotBeNull(),
                () => response.Result.ShouldNotBeNull(),
                () => response.Result.Data.ShouldNotBeNull(),
                () => response.Result.Error.ShouldBeNull(),
                () => response.Result.Data.Id.ShouldBe(randomId));
        }
        
        [Test]
        public void CreateClient_Incorrect_Name()
        {
            // Arrange
            var randomId = new Random().Next(0, 10000);
            var clients = new List<Client>();
            
            var context = new Mock<ZelfContext>();
            context.Setup(x => x.Clients.AddAsync(It.IsAny<Client>(), It.IsAny<CancellationToken>()))
                .Callback((Client model, CancellationToken token) =>
                {
                    model.Id = randomId;
                    clients.Add(model);
                });
                        
            var contextFactory = new Mock<IContextFactory>();
            contextFactory
                .Setup(x => x.Create())
                .Returns(context.Object);

            var api = new Core.ClientsApi(contextFactory.Object);

            var request = new CreateClientRequest
            {
                Name = "Аркадий Петрович"
            };

            // Act
            var response = api.CreateClientAsync(request);

            // Assert
            response.ShouldSatisfyAllConditions(
                () => response.ShouldNotBeNull(),
                () => response.Result.ShouldNotBeNull(),
                () => response.Result.Data.ShouldNotBeNull(),
                () => response.Result.Error.ShouldBeNull(),
                () => response.Result.Data.Id.ShouldBe(randomId));
        }
    }
}