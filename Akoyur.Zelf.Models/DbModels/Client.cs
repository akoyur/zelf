﻿using System;
using System.Collections.Generic;
using Akoyur.Zelf.Models.DbModels.Base;

namespace Akoyur.Zelf.Models.DbModels
{
    /// <summary>
    /// Клиент
    /// </summary>
    public class Client : Entity
    {
        /// <summary>
        /// Имя клиента
        /// </summary>
        public string Name { get; set; }
        
        /// <summary>
        /// Дата обновления клиента
        /// </summary>
        public DateTime? UpdatedAt { get; set; }
        
        /// <summary>
        /// Количество клиентов, на которых данный клиент подписан
        /// </summary>
        public int SubscriptionsCount { get; set; }
        
        /// <summary>
        /// Количество клиентов, подписанных на данного клиента
        /// </summary>
        public int SubscribersCount { get; set; }
        
        /// <summary>
        /// Список клиентов, на которых данный клиент подписан 
        /// </summary>
        public virtual IEnumerable<Subscription> Subscriptions { get; set; }
        
        /// <summary>
        /// Список клиентов, подписанных на данного клиента 
        /// </summary>
        public virtual IEnumerable<Subscription> Subscribers { get; set; }
    }
}