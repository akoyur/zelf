﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Akoyur.Zelf.Models.DbModels.Base
{
    /// <summary>
    /// Базовый класс сущности базы данных
    /// </summary>
    public abstract class Entity
    {
        /// <summary>
        /// Идентификатор сущности
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public  int Id { get; set; }
        
        /// <summary>
        /// Дата создания сущности
        /// </summary>
        public DateTime CreatedAt { get; set; }
        
        /// <summary>
        /// Дата удаления сущности
        /// </summary>
        public DateTime? DeletedAt { get; set; }
        
        /// <summary>
        /// Признак, что сущность удалена
        /// </summary>
        public bool IsDeleted { get; set; }
    }
}