﻿using Akoyur.Zelf.Models.DbModels.Base;

namespace Akoyur.Zelf.Models.DbModels
{
    public class Subscription : Entity
    {
        /// <summary>
        /// 
        /// </summary>
        public int ClientId { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public int SubscriberId { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public Client Client { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public Client Subscriber { get; set; }
    }
}