﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Akoyur.Zelf.API.Interfaces;
using Akoyur.Zelf.API.Models.v1;
using Akoyur.Zelf.API.Models.v1.Requests.Clients;
using Akoyur.Zelf.API.Models.v1.Responses.Base;
using Akoyur.Zelf.API.Models.v1.Responses.Clients;
using Akoyur.Zelf.Enumerations;
using Akoyur.Zelf.Models.DbModels;
using Akoyur.Zelf.Storage;
using Microsoft.EntityFrameworkCore;

namespace Akoyur.Zelf.Core
{
    /// <inheritdoc cref="IClientsApi"/>
    public class ClientsApi : IClientsApi
    {
        private readonly IContextFactory _contextFactory;

        public ClientsApi(IContextFactory contextFactory)
        {
            _contextFactory = contextFactory;
        }

        /// <inheritdoc cref="IClientsApi.GetClientAsync"/>
        public async Task<BaseResponse<GetClientResponse>> GetClientAsync(int id)
        {
	        await using var context = _contextFactory.Create();

	        var client = context.Clients
		        .AsNoTracking()
		        .Where(x => x.Id == id && !x.IsDeleted)
		        .Select(x => new GetClientResponse
		        {
			        Id = x.Id,
			        Name = x.Name,
			        CreatedAt = x.CreatedAt,
			        UpdatedAt = x.UpdatedAt,
			        SubscribersCount = x.SubscribersCount,
			        SubscriptionsCount = x.SubscriptionsCount
		        })
		        .FirstOrDefault();
	        
	        if (client == null)
		        return new BaseResponse<GetClientResponse>($"Клиент с идентификатором '{id}' не найден.");
	        
	        return new BaseResponse<GetClientResponse>(client);
        }

        /// <inheritdoc cref="IClientsApi.CreateClientAsync"/>
        public async Task<BaseResponse<CreateClientResponse>> CreateClientAsync(CreateClientRequest model)
        {
	        var result = new BaseResponse<CreateClientResponse>();

	        await using var context = _contextFactory.Create();

	        var client = new Client
	        {
		        Name = model.Name,
		        CreatedAt = DateTime.UtcNow
	        };

	        await context.Clients.AddAsync(client);
	        await context.SaveChangesAsync();

	        result.Data = new CreateClientResponse
	        {
		        Id = client.Id,
		        Name = client.Name,
		        CreatedAt = client.CreatedAt,
		        SubscribersCount = client.SubscribersCount,
		        SubscriptionsCount = client.SubscriptionsCount
	        };
	        
	        return result;
        }

        /// <inheritdoc cref="IClientsApi.DeleteClientAsync"/>
        public async Task<BaseResponse<DeleteClientResponse>> DeleteClientAsync(DeleteClientRequest model)
        {
	        await using var context = _contextFactory.Create();

	        var client = context.Clients.FirstOrDefault(x => x.Id == model.Id && !x.IsDeleted);
	        if (client == null)
		        return new BaseResponse<DeleteClientResponse>($"Клиент с идентификатором {nameof(model.Id)}='{model.Id}' не найден.");

	        client.IsDeleted = true;
	        client.DeletedAt = DateTime.UtcNow;

	        context.Clients.Update(client);
	        await context.SaveChangesAsync();
	        return new BaseResponse<DeleteClientResponse>();
        }

        /// <inheritdoc cref="IClientsApi.GetClientsAsync"/>
        public async Task<BaseResponse<PagingResultModel<GetClientsResponse>>> GetClientsAsync(GetClientsRequest model)
        {
	        var response = new BaseResponse<PagingResultModel<GetClientsResponse>>();

			await using var context = _contextFactory.Create();

			var query = context.Clients.AsNoTracking().Where(x => !x.IsDeleted);
			int? linksOfClientId = null;

			if (model.Where?.Length > 0)
			{
				foreach (var whereField in model.Where)
				{
					switch (whereField.FieldName)
					{
						case "Name":
						{
							query = query.WhereStringField(
								(source, value) => source.Where(x => x.Name != null && x.Name == value),
								(source, value) => source.Where(x => x.Name != null && x.Name.Contains(value)),
								whereField, response);
							break;
						}
						case "LinksOfClientId":
						{
							if (!int.TryParse(whereField.FieldValue, out var value))
								return new BaseResponse<PagingResultModel<GetClientsResponse>>($"Некорректное значение фильтрации. Ожидается целочисленное значение идентификатора клиента.");
							
							linksOfClientId = value;
							query = query.Where(x => x.Id != value);
							
							break;
						}
						default:
						{
							return new BaseResponse<PagingResultModel<GetClientsResponse>>($"Некорректное значение фильтрации. Доступные значения: Name, LinksOfClientId.");
						}
					}
				}
				
				if (response.Error != null)
					return response;
			}

			var total = await query.CountAsync();
			IOrderedQueryable<Client> orderedQuery = null;

			if (model.Sort?.Length > 0)
			{
				foreach (var sortField in model.Sort)
				{
					if (!Enum.IsDefined(typeof(SortEnum), sortField.Type))
						return new BaseResponse<PagingResultModel<GetClientsResponse>>($"Некорректное значение типа сортировки. Доступные значения: Asc, Desc.");
					
					switch (sortField.FieldName)
					{
						case "UpdatedAt":
						{
							orderedQuery = sortField.Type == SortEnum.Asc
								? orderedQuery == null ? query.OrderBy(x => x.UpdatedAt) : orderedQuery.ThenBy(x => x.UpdatedAt)
								: orderedQuery == null ? query.OrderByDescending(x => x.UpdatedAt) : orderedQuery.ThenByDescending(x => x.UpdatedAt);
							break;
						}
						case "CreatedAt":
						{
							orderedQuery = sortField.Type == SortEnum.Asc
								? orderedQuery == null ? query.OrderBy(x => x.CreatedAt) : orderedQuery.ThenBy(x => x.CreatedAt)
								: orderedQuery == null ? query.OrderByDescending(x => x.CreatedAt) : orderedQuery.ThenByDescending(x => x.CreatedAt);
							break;
						}
						case "SubscribersCount":
						{
							orderedQuery = sortField.Type == SortEnum.Asc
								? orderedQuery == null ? query.OrderBy(x => x.SubscribersCount) : orderedQuery.ThenBy(x => x.SubscribersCount)
								: orderedQuery == null ? query.OrderByDescending(x => x.SubscribersCount) : orderedQuery.ThenByDescending(x => x.SubscribersCount);
							break;
						}
						case "SubscriptionsCount":
						{
							orderedQuery = sortField.Type == SortEnum.Asc
								? orderedQuery == null ? query.OrderBy(x => x.SubscriptionsCount) : orderedQuery.ThenBy(x => x.SubscriptionsCount)
								: orderedQuery == null ? query.OrderByDescending(x => x.SubscriptionsCount) : orderedQuery.ThenByDescending(x => x.SubscriptionsCount);
							break;
						}
						default:
						{
							return new BaseResponse<PagingResultModel<GetClientsResponse>>($"Некорректное значение сортировки. Доступные значения: CreatedAt, UpdatedAt, SubscribersCount, SubscriptionsCount.");
						}
					}
				}
			}
			else
			{
				orderedQuery = query.OrderByDescending(x => x.Id);
			}

			var list = await orderedQuery
				.Skip(model.Paging.Skip)
				.Take(model.Paging.Take)
				.Select(x => new GetClientsResponse
				{
					Id = x.Id,
					Name = x.Name,
					CreatedAt = x.CreatedAt,
					UpdatedAt = x.UpdatedAt,
					SubscribersCount = x.SubscribersCount,
					SubscriptionsCount = x.SubscriptionsCount
				})
				.ToListAsync();

			if (linksOfClientId.HasValue && list.Any())
			{
				var clientIds = list.Select(x => x.Id).ToList();
				var subscriptions = await context.Subscriptions
					.AsNoTracking()
					.Where(x => !x.IsDeleted && x.SubscriberId == linksOfClientId.Value && clientIds.Contains(x.ClientId))
					.Select(x => new
					{
						x.Id,
						x.ClientId
					})
					.ToDictionaryAsync(x => x.ClientId, x => x.Id);

				list.ForEach(item =>
				{
					item.SubscriptionId = subscriptions.TryGetValue(item.Id, out var subscriptionId) ? subscriptionId : (int?) null;
				});
			}
			
			return new BaseResponse<PagingResultModel<GetClientsResponse>>(new PagingResultModel<GetClientsResponse>(list, total));
        }

        /// <inheritdoc cref="IClientsApi.SubscribeClientAsync"/>
        public async Task<BaseResponse<SubscribeClientResponse>> SubscribeClientAsync(SubscribeClientRequest model)
        {
	        var result = new BaseResponse<SubscribeClientResponse>();

	        await using var context = _contextFactory.Create();
	        await using var transaction = await context.Database.BeginTransactionAsync();

	        var subscriptionExists = context
		        .Subscriptions
		        .Any(x => x.ClientId == model.ClientId && x.SubscriberId == model.SubscriberId && !x.IsDeleted);

	        if (subscriptionExists)
		        return new BaseResponse<SubscribeClientResponse>($"Клиент с идентификатором '{model.SubscriberId}' уже подписан на клиента с идентификатором '{model.ClientId}'.");

	        var clientExists = context.Clients.Any(x => x.Id == model.ClientId && !x.IsDeleted);
	        if (!clientExists)
		        return new BaseResponse<SubscribeClientResponse>($"Клиент с идентификатором '{model.ClientId}' не найден.");

	        var subscriberExists = context.Clients.Any(x => x.Id == model.SubscriberId && !x.IsDeleted);
	        if (!subscriberExists)
		        return new BaseResponse<SubscribeClientResponse>($"Клиент с идентификатором '{model.SubscriberId}' не найден.");

	        var subscription = new Subscription
	        {
		        ClientId = model.ClientId,
		        CreatedAt = DateTime.UtcNow,
		        SubscriberId = model.SubscriberId
	        };

	        // Сохраняем подписку
	        await context.Subscriptions.AddAsync(subscription);

	        // Уменьшаем на 1 счетчик подписчиков клиента
	        await context.Database.ExecuteSqlRawAsync(@$"
					UPDATE {nameof(context.Clients)} 
					SET {nameof(Client.SubscribersCount)}={nameof(Client.SubscribersCount)}+1
					WHERE {nameof(Client.Id)}={model.ClientId}");

	        // Уменьшаем на 1 счетчик подписок у подписчика
	        await context.Database.ExecuteSqlRawAsync(@$"
					UPDATE {nameof(context.Clients)} 
					SET {nameof(Client.SubscriptionsCount)}={nameof(Client.SubscriptionsCount)}+1
					WHERE {nameof(Client.Id)}={model.SubscriberId}");

	        await transaction.CommitAsync();
	        await context.SaveChangesAsync();

	        result.Data = new SubscribeClientResponse
	        {
		        Id = subscription.Id,
		        CreatedAt = subscription.CreatedAt
	        };

	        return result;
        }

        /// <inheritdoc cref="IClientsApi.UnSubscribeClientAsync"/>
        public async Task<BaseResponse<UnSubscribeClientResponse>> UnSubscribeClientAsync(UnSubscribeClientRequest model)
        {
	        await using var context = _contextFactory.Create();
	        await using var transaction = await context.Database.BeginTransactionAsync();
	        
	        var subscription = context
		        .Subscriptions
		        .FirstOrDefault(x => x.ClientId == model.ClientId && x.SubscriberId == model.SubscriberId && !x.IsDeleted);
	        
	        if (subscription == null)
		        return new BaseResponse<UnSubscribeClientResponse>($"Подписка не найдена.");
       
	        subscription.DeletedAt = DateTime.UtcNow;
	        subscription.IsDeleted = true;
	        
	        context.Subscriptions.Update(subscription);
	        
	        // Увелииваем на 1 счетчик подписчиков клиента
	        await context.Database.ExecuteSqlRawAsync(@$"
					UPDATE {nameof(context.Clients)} 
					SET {nameof(Client.SubscribersCount)}={nameof(Client.SubscribersCount)}-1 
					WHERE {nameof(Client.Id)}={model.ClientId}");
	        
	        // Увеличиваем на 1 счетчик подписок у подписчика
	        await context.Database.ExecuteSqlRawAsync(@$"
					UPDATE {nameof(context.Clients)} 
					SET {nameof(Client.SubscriptionsCount)}={nameof(Client.SubscriptionsCount)}-1 
					WHERE {nameof(Client.Id)}={model.SubscriberId}");
	        
	        await transaction.CommitAsync();
	        await context.SaveChangesAsync();
	        
	        return new BaseResponse<UnSubscribeClientResponse>();
        }

        /// <inheritdoc cref="IClientsApi.UpdateClientAsync"/>
        public async Task<BaseResponse<UpdateClientResponse>> UpdateClientAsync(UpdateClientRequest model)
        {
	        var result = new BaseResponse<UpdateClientResponse>();
            
	        await using var context = _contextFactory.Create();
            
	        var client = context.Clients.FirstOrDefault(x => x.Id == model.Id && !x.IsDeleted);
	        if (client == null)
		        return new BaseResponse<UpdateClientResponse>($"Клиент с идентификатором {nameof(model.Id)}='{model.Id}' не найден.");

	        client.Name = model.Name;
	        client.UpdatedAt = DateTime.UtcNow;

	        context.Clients.Update(client);
	        await context.SaveChangesAsync();

	        result.Data = new UpdateClientResponse
	        {
		        Id = client.Id,
		        Name = client.Name,
		        CreatedAt = client.CreatedAt,
		        UpdatedAt = client.UpdatedAt,
		        SubscribersCount = client.SubscribersCount,
		        SubscriptionsCount = client.SubscriptionsCount
	        };

	        return result;
        }
    }
}