﻿using Akoyur.Zelf.API.Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace Akoyur.Zelf.Core.IoC
{
    /// <summary>
    /// Класс построитель DI
    /// </summary>
    public static class CoreDI
    {
        /// <summary>
        /// Метод расширения для добавления зависимостей и создания IoC контейнера
        /// </summary>
        public static IServiceCollection AddCoreDependencies(this IServiceCollection collection)
        {
            collection.AddTransient<IClientsApi, ClientsApi>();
            return collection;
        }
    }
}