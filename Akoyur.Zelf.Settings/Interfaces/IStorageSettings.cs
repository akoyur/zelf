﻿namespace Akoyur.Zelf.Settings.Interfaces
{
    /// <summary>
    /// Класс Storage-настроек 
    /// </summary>
    public interface IStorageSettings
    {
        /// <summary>
        /// Имя файла БД SQlite 
        /// </summary>
        string Filename { get; }
    }
}