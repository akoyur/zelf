﻿using System;
using Akoyur.Zelf.Settings.Interfaces;
using Microsoft.Extensions.Configuration;

namespace Akoyur.Zelf.Settings
{
    /// <inheritdoc cref="IStorageSettings"/>
    public class StorageSettings : IStorageSettings
    {
        /// <inheritdoc cref="IStorageSettings.Filename" />
        public string Filename { get; }

        public StorageSettings(string filename)
        {
            Filename = filename;
        }
        
        public StorageSettings(IConfiguration configuration)
        {
            var configurationValue = configuration.GetSection("Storage:SQlite:Filename")?.Value;
            Filename = string.IsNullOrWhiteSpace(configurationValue) ? "Akoyur.Zelf.db" : configurationValue;
        }
    }
}