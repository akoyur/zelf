﻿using Akoyur.Zelf.Settings.Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace Akoyur.Zelf.Settings.IoC
{
    /// <summary>
    /// Класс построитель DI
    /// </summary>
    public static class SettingsDI
    {
        /// <summary>
        /// Метод расширения для добавления зависимостей и создания IoC контейнера
        /// </summary>
        public static IServiceCollection AddSettingsDependencies(this IServiceCollection collection)
        {
            collection.AddSingleton<IStorageSettings, StorageSettings>();
            return collection;
        }
    }
}