﻿# Видео
Демонстрационный ролик: https://www.youtube.com/watch?v=3noW6FpBKwE

# Тестовое задание
Разработать WebAPI сервис для соцсети, который позволяет сделать следующее: 
- Зарегистрировать нового клиента. Клиент при регистрации указывает только имя. Имя должно состоять из букв и пробелов и быть не длиннее 64 символов 
- Подписать одного клиента на другого
- Выбрать топ наиболее популярных клиентов. Вызывающий может указать требуемое количество записей

Сервис внутренний, авторизации клиентов не требуется. 
Структура базы, выбор библиотек и контрактов сервиса остаются за вами. При неясностях в бизнес требованиях или в реализации выбирать более простое решение, исходя из здравого смысла. Сервис должен быть разработан с использованием фреймворка .NET Core. Данные сохранять в персистентное хранилище - SQLite. Приложение должно запускаться без дополнительной конфигурации или установки дополнительного ПО. 

# Структура решения 

|First Header| Second Header|
|------------ | ------------ |
|*API:*|
|Akoyur.Zelf.API|WebApi для SPA-приложения|
|Akoyur.Zelf.API.Interfaces|Интерфейсы API|
|Akoyur.Zelf.API.Models|Модели (ViewModels) API. Содержит модели всех запросов и ответов.|
|*TESTS:*||
|Akoyur.Zelf.API.Models.UnitTests |Пример Unit-тестов моделей API|
|Akoyur.Zelf.Core.IntegrationTests|Пример интеграционных тестов на локальной тестовой базе данных|
|Akoyur.Zelf.Core.UnitTests|Пример юнит тестов с Mock-логикой|
|Akoyur.Zelf.Helpers.UnitTests|Пример Unit-тестов вспомогательного кода|
|*WEB:*||
|Akoyur.Zelf.WEB|SPA-приложение (Angular)|
|*CORE:*||
|Akoyur.Zelf.Core|Код бизнес-логики|
|Akoyur.Zelf.Enumerations|Библиотека перечислений|
|Akoyur.Zelf.Helpers|Библиотека вспомогательного кода|
|Akoyur.Zelf.Models|Модели DAL (Entities)|
|Akoyur.Zelf.Settings|Библиотека настроек|
|Akoyur.Zelf.Storage|Библиотека DAL|

# Структура проекта
## SPA-приложение
SPA-приложение Akoyur.Zelf.WEB представляет из себя Angular 12 приложение.

Включает в себя компонены для работы с базой клиентов
- Список клиентов и функционал подписки/отписки (clients/client-list.component)
- Регистрация нового клиента (clients/client-add.component)
- Редактирование и удаление существующего клиента (clients/client-edit.component)
- Форма просмотра клиента (clients/client-from.component)

Включает в себя модели:
- Модель представления клиента Client (models/client)
- Модели поисковых параметров и перечислений SearchFilter, SearchFilterPaging, SearchFilterSortField, SearchFilterWhereField, SortEnum, WhereEnum (clients/models/client)

Включает в себя:
- Сервис ClientsService для интеграции с API (services/clients.service)

## API
API-приложение Akoyur.Zelf.API реализовано на базе WebApi и предоставляет доступ клиентам к ресурсу `clients`.

HostController - базовый класс контроллера, реализующий логику валидации и вызова метода
ValidatableRequestModel - базовый класс моделей запросов API, в каждом из которых реализована логика валидации входящих параметров

- Базовая модель ответа API:
```
{
    "data": <object> - Объект-результат 
    "error": {
        "message": <string>, - Базовое сообщение об ошибке
        "errors": [<key,value>,...] - Словарь ошибок (ключ-значение), для отображения валидационных и иных ошибок
    }
}
```

- Зарегистрировать нового клиента:
```
HTTP POST /api/v1/clients 

Запрос:
{
    "name": <string> - Имя клиента
}

Ответ (data):
{
    "id": <integer>, - Идентификатор клиента
    "name": <string>, - Имя клиента
    "createdAt": <datetime>, - Дата регистрации клиента 
    "subscriptionsCount": <integer>, - Количество подписок клиента
    "subscribersCount": <integer> - Количество подписчиков клиента
}
```

- Обновить клиента:
```
HTTP POST /api/v1/clients/{clientId}

Запрос:
{
    "name": <string> - Имя клиента
}

Ответ (data):
{
    "id": <integer>, - Идентификатор клиента
    "name": <string>, - Имя клиента
    "createdAt": <datetime>, - Дата регистрации клиента 
    "updatedAt": <datetime>, - Дата последнего обновления клиента
    "subscriptionsCount": <integer>, - Количество подписок клиента
    "subscribersCount": <integer> - Количество подписчиков клиента
}
```


- Получить информацию о клиенте:
```
HTTP GET /api/v1/clients/{clientId}

Запрос:
{
    "name": <string> - Имя клиента
}

Ответ (data):
{
    "id": <integer>, - Идентификатор клиента
    "name": <string>, - Имя клиента
    "createdAt": <datetime>, - Дата регистрации клиента 
    "updatedAt": <datetime>, - Дата последнего обновления клиента
    "subscriptionsCount": <integer>, - Количество подписок клиента
    "subscribersCount": <integer> - Количество подписчиков клиента
}
```


- Получить список клиентов:
Модель поискового запроса реализована на основе массивов Sort и Where,
которые позволяют создавать динамические запросы без изменения контракта API.
```
HTTP POST /api/v1/clients/search

Запрос:
{
    "paging": { - Параметры постраничного отображения
        "skip": <integer>, - Количество элементов, которые пропустить
        "take": <integer> - Количество элементов для возврата в коллекции
    },
    "sort": [{
        "type": <enum>, // SortEnum: { Asc, Desc } - Тип сортировки
        "fieldName": <string> - Поле, по которому необходима сортировка
    },...],
    "where": [{
        "type": <enum>, // WhereEnum - Тип условного выражения
        "fieldName": <string>, - Наименование поля фильтрации
        "fieldValue": <string>, - Значение поля
        "fieldValues": <string[]> - Значение поля (коллекция)
    },...]
}

Ответ (data):
{
    "id": <integer>,
    "name": <string>,
    "createdAt": <datetime>,
    "updatedAt": <datetime>,
    "subscriptionsCount": <integer>,
    "subscribersCount": <integer>,
    "subscriptionId": <integer?> - При фильтрации по пользовательским связям: Идентификатор подписки данного клиента на клиента фильтрации
}
```


- Удалить клиента:
```
HTTP DELETE /api/v1/clients/{clientId}

Запрос:
-

Ответ (data):
-
```


- Подписать одного клиента на другого:
```
HTTP POST /api/v1/clients/{clientId}/subscriptions

Запрос:
{
    "clientId": <integer>, - Уникальный идентификатор клиента
    "subscriberId": <integer>, - Уникальный идентификатор клиента-подписчика
}

Ответ (data):
{
    "id": <integer>, - Уникальный идентификатор подписки
    "createdAt": <datetime> - Дата создания подписки
}
```


- Отписать одного клиента от другого:
```
HTTP DELETE /api/v1/clients/{clientId}/subscriptions/{subscriptionId}

Запрос:
-

Ответ (data):
-
```

## Бизнес-логика
Бизнес-логика реализована в ClientsApi (IClientsApi).
С целью предотвращения двойной валидации входных параметров, предполагается,
что модели методов валидируются в контроллере API.

## База данных SQLite

Для доступа к БД используется EF Core
Уровель DAL реализован при помощи:
- IStorageSettings, StorageSettings - Настройки БД
- IContextFactory, ContextFactory - Класс-фабрика DbContext
- ZelfContext - Класс DbContext

```sql
CREATE TABLE "Clients" (
    "Id" INTEGER NOT NULL CONSTRAINT "PK_Clients" PRIMARY KEY AUTOINCREMENT,
    "CreatedAt" TEXT NOT NULL,
    "DeletedAt" TEXT NULL,
    "IsDeleted" INTEGER NOT NULL,
    "Name" TEXT NULL,
    "UpdatedAt" TEXT NULL,
    "SubscriptionsCount" INTEGER NOT NULL,
    "SubscribersCount" INTEGER NOT NULL
)

CREATE TABLE "Subscriptions" (
    "Id" INTEGER NOT NULL CONSTRAINT "PK_Subscriptions" PRIMARY KEY AUTOINCREMENT,
    "CreatedAt" TEXT NOT NULL,
    "DeletedAt" TEXT NULL,
    "IsDeleted" INTEGER NOT NULL,
    "ClientId" INTEGER NOT NULL,
    "SubscriberId" INTEGER NOT NULL,
    CONSTRAINT "FK_Subscriptions_Clients_ClientId" FOREIGN KEY ("ClientId") REFERENCES "Clients" ("Id") ON DELETE CASCADE,
    CONSTRAINT "FK_Subscriptions_Clients_SubscriberId" FOREIGN KEY ("SubscriberId") REFERENCES "Clients" ("Id") ON DELETE CASCADE
)
```