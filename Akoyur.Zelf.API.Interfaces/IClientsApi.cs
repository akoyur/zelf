﻿using System.Threading.Tasks;
using Akoyur.Zelf.API.Models.v1.Responses.Base;
using Akoyur.Zelf.API.Models.v1.Requests.Clients;
using Akoyur.Zelf.API.Models.v1.Responses.Clients;

namespace Akoyur.Zelf.API.Interfaces
{
    /// <summary>
    /// Интерфейс API "Клиенты"
    /// </summary>
    public interface IClientsApi
    {
	    /// <summary>
	    /// Получить информацию о клиенте
	    /// </summary>
	    /// <param name="id">Идентификатор клиента</param>
	    /// <returns>Модель ответа</returns>
	    Task<BaseResponse<GetClientResponse>> GetClientAsync(int id);
	    
        /// <summary>
		/// Зарегистрировать нового клиента
		/// </summary>
		/// <param name="model">Модель запроса</param>
		/// <returns>Модель ответа</returns>
		Task<BaseResponse<CreateClientResponse>> CreateClientAsync(CreateClientRequest model);
		
		/// <summary>
		/// Удалить клиента
		/// </summary>
		/// <param name="model">Модель запроса</param>
		/// <returns>Модель ответа</returns>
		Task<BaseResponse<DeleteClientResponse>> DeleteClientAsync(DeleteClientRequest model);

		/// <summary>
		/// Получить список клиентов
		/// </summary>
		/// <param name="model">Модель запроса</param>
		/// <returns>Модель ответа</returns>
		Task<BaseResponse<PagingResultModel<GetClientsResponse>>> GetClientsAsync(GetClientsRequest model);
        
        /// <summary>
		/// Подписать одного клиента на другого
		/// </summary>
		/// <param name="model">Модель запроса</param>
		/// <returns>Модель ответа</returns>
		Task<BaseResponse<SubscribeClientResponse>> SubscribeClientAsync(SubscribeClientRequest model);
		
		/// <summary>
		/// Отписать одного клиента от другого
		/// </summary>
		/// <param name="model">Модель запроса</param>
		/// <returns>Модель ответа</returns>
		Task<BaseResponse<UnSubscribeClientResponse>> UnSubscribeClientAsync(UnSubscribeClientRequest model);
		
		/// <summary>
		/// Обновить клиента
		/// </summary>
		/// <param name="model">Модель запроса</param>
		/// <returns>Модель ответа</returns>
		Task<BaseResponse<UpdateClientResponse>> UpdateClientAsync(UpdateClientRequest model);
    }
}