﻿using Microsoft.Extensions.DependencyInjection;

namespace Akoyur.Zelf.Storage.IoC
{
    /// <summary>
    /// Класс построитель DI
    /// </summary>
    public static class StorageDI
    {
        /// <summary>
        /// Метод расширения для добавления зависимостей и создания IoC контейнера
        /// </summary>
        public static IServiceCollection AddStorageDependencies(this IServiceCollection collection)
        {
            collection.AddEntityFrameworkSqlite();
            collection.AddSingleton<IContextFactory, ContextFactory>();
            return collection;
        }
    }
}