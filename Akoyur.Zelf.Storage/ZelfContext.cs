﻿using Akoyur.Zelf.Models.DbModels;
using Microsoft.EntityFrameworkCore;

namespace Akoyur.Zelf.Storage
{
	/// <summary>
	/// Класс DbContext
	/// </summary>
	public class ZelfContext : DbContext
	{
		#region .ctor

		/// <summary>
		/// .ctor
		/// </summary>
		protected ZelfContext()
		{
			//
		}

		/// <summary>
		/// .ctor
		/// </summary>
		public ZelfContext(DbContextOptions options) : base(options)
		{
			//
		}

		/// <summary>
		/// .ctor
		/// </summary>
		public ZelfContext(DbContextOptions<ZelfContext> options) : base(options)
		{
			//
		}

		#endregion

		/// <summary>
		/// Таблица "Клиенты"
		/// </summary>
		public virtual DbSet<Client> Clients { get; set; }

		/// <summary>
		/// Таблица "Подписки"
		/// </summary>
		public virtual DbSet<Subscription> Subscriptions { get; set; }

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			modelBuilder.Entity<Client>().ToTable("Clients", "dbo");
			modelBuilder.Entity<Subscription>().ToTable("Subscriptions", "dbo");

			modelBuilder.Entity<Subscription>()
				.HasOne(subscription => subscription.Client)
				.WithMany(client => client.Subscriptions)
				.HasForeignKey(subscription => subscription.ClientId);

			modelBuilder.Entity<Subscription>()
				.HasOne(subscription => subscription.Subscriber)
				.WithMany(client => client.Subscribers)
				.HasForeignKey(subscription => subscription.SubscriberId);
			
			base.OnModelCreating(modelBuilder);
		}
	}
}