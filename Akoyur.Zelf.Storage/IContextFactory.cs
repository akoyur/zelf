﻿using Akoyur.Zelf.Settings.Interfaces;

namespace Akoyur.Zelf.Storage
{
    /// <summary>
    /// Интерфейс фабрики создания БД
    /// </summary>
    public interface IContextFactory
    {
        /// <summary>
        /// Настройки
        /// </summary>
        IStorageSettings StorageSettings { get; }

        /// <summary>
        /// Метод создания контекста
        /// </summary>
        ZelfContext Create();
    }
}