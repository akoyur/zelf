﻿using System;
using Akoyur.Zelf.Settings.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Akoyur.Zelf.Storage
{
    /// <summary>
    /// Класс-фабрика DbContext
    /// </summary>
    public class ContextFactory : IContextFactory
    {
        private DbContextOptions<ZelfContext> _options;

        /// <inheritdoc cref="IContextFactory.StorageSettings"/>
        public IStorageSettings StorageSettings { get; }

        /// <summary>
        /// Конструктор фабрики создания констекста
        /// </summary>
        /// <param name="settings">Настройки</param>
        public ContextFactory(IStorageSettings settings)
        {
            StorageSettings = settings ?? throw new ArgumentNullException(nameof(settings));
            SetOptions();
        }

        /// <inheritdoc cref="IContextFactory.Create"/>
        public ZelfContext Create()
        {
            var context = new ZelfContext(_options);
            context.Database.EnsureCreated();
            return context;
        }

        /// <summary>
        /// Установить конфигурацию
        /// </summary>
        private void SetOptions()
        {
            var builder = new DbContextOptionsBuilder<ZelfContext>();
            builder.UseSqlite($"Filename={StorageSettings.Filename}");
            _options = builder.Options;
        }
    }
}