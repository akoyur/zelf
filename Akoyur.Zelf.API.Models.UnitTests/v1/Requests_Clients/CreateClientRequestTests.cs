using System.Linq;
using Akoyur.Zelf.API.Models.v1.Requests.Clients;
using NUnit.Framework;
using Shouldly;

namespace Akoyur.Zelf.API.Models.UnitTests.v1.Requests_Clients
{
    public class CreateClientRequestTests
    {
        [Test(Description = "Некорректный запрос регистрации клиента: передан Name=NULL")]
        public void CreateClientRequest_Null_Name()
        {
            // Arrange
            var model = new CreateClientRequest();
            
            // Act
            var isValid = model.IsValid(out var errors);
            
            // Assert
            isValid.ShouldBeFalse();
            errors.ShouldSatisfyAllConditions(
                () => errors.ShouldNotBeNull(),
                () => errors.Keys.Count.ShouldBe(1),
                () => errors.Keys.First().ShouldBe("name"),
                () => errors.Values.First().ShouldContain("не задано или некорректно")
            );
        }
        
        [Test(Description = "Корректный запрос регистрации клиента")]
        [TestCase("Иванов")]
        [TestCase("Иванов Артём")]
        [TestCase("Иванов Артём Олегович")]
        public void CreateClientRequest_Success(string name)
        {
            // Arrange
            var model = new CreateClientRequest
            {
                Name = name
            };
            
            // Act
            var isValid = model.IsValid(out var errors);
            
            // Assert
            isValid.ShouldBeTrue();
            errors.ShouldSatisfyAllConditions(
                () => errors.ShouldNotBeNull(),
                () => errors.Keys.Count.ShouldBe(0)
            );
        }
        
        [Test(Description = "Некорректный запрос регистрации клиента")]
        [TestCase("1")]
        [TestCase("Иванов 1")]
        [TestCase("Иванов #")]
        [TestCase("ИвановИвановИвановИвановИвановИвановИвановИвановИвановИвановИвановИвановИвановИвановИвановИвановИвановИвановИвановИванов")]
        public void CreateClientRequest_ValidationError(string name)
        {
            // Arrange
            var model = new CreateClientRequest
            {
                Name = name
            };
            
            // Act
            var isValid = model.IsValid(out var errors);
            
            // Assert
            isValid.ShouldBeFalse();
            errors.ShouldSatisfyAllConditions(
                () => errors.ShouldNotBeNull(),
                () => errors.Keys.Count.ShouldBe(1),
                () => errors.Keys.First().ShouldBe("name"),
                () => errors.Values.First().ShouldContain("Имя должно состоять из букв и пробелов и быть не длиннее 64 символов.")
            );
        }
    }
}