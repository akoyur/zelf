﻿using System;
using System.IO;
using Akoyur.Zelf.Settings;
using Akoyur.Zelf.Settings.Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace Akoyur.Zelf.Core.IntegrationTests.Base
{
    public abstract class LocalDbBaseTests
    {
        protected const string TestDbFileName = "Akoyur.Zelf.Core.IntegrationTests.db";

        /// <summary>Сервис провайдер</summary>
        protected IServiceProvider Provider { get; private set; }

        /// <summary>Добавление зависимостей в IoC контейнер</summary>
        /// <param name="services"></param>
        /// <param name="localDbFileName">Имя файла базы данных</param>
        protected abstract void ConfigureServices(
            IServiceCollection services,
            string localDbFileName);

        /// <summary>Однократная установка</summary>
        /// <returns>Задача завершения</returns>
        [NUnit.Framework.OneTimeSetUp]
        public void TestOneTimeSetUp() => this.OneTimeSetUp();

        /// <summary>Однократная очистка</summary>
        /// <returns>Задача завершения</returns>
        [NUnit.Framework.OneTimeTearDown]
        public void TestOneTimeTearDown() => OneTimeTearDown();
        
        /// <summary>Базовый метод настройки тестов</summary>
        /// <returns>Задача завершения</returns>
        protected virtual void OneTimeSetUp()
        {
            Provider = BuildServiceProvider(TestDbFileName);
            DropTestDatabases();
        }

        protected void DropTestDatabases()
        {
            var path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, TestDbFileName);

            if (File.Exists(path))
                File.Delete(path);
        }

        /// <summary>Базовый метод однократной очистки тестов</summary>
        /// <returns>Задача завершения</returns>
        protected virtual void OneTimeTearDown()
        {
            DropTestDatabases();
        }
        
        /// <summary>Построить провайдер сервисов</summary>
        /// <param name="localDbFileName">Имя файла базы данных</param>
        /// <returns>провайдер сервисов</returns>
        protected IServiceProvider BuildServiceProvider(string localDbFileName)
        {
            IServiceCollection services = (IServiceCollection) new ServiceCollection();
            this.ConfigureServices(services, localDbFileName);
            return (IServiceProvider) ServiceCollectionContainerBuilderExtensions.BuildServiceProvider(services);
        }

        protected IStorageSettings GetLocalDbSettings()
        {
            return new StorageSettings(TestDbFileName);
        }
    }
}