﻿using System.Threading.Tasks;
using Akoyur.Zelf.API.Interfaces;
using Akoyur.Zelf.API.Models.v1.Requests.Base;
using Akoyur.Zelf.API.Models.v1.Requests.Clients;
using Akoyur.Zelf.Core.IntegrationTests.Base;
using Akoyur.Zelf.Core.IoC;
using Akoyur.Zelf.Enumerations;
using Akoyur.Zelf.Storage.IoC;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;
using Shouldly;

namespace Akoyur.Zelf.Core.IntegrationTests
{
    public class GetClientsTests : LocalDbBaseTests
    {
        // Настройка сервисов (Локальная База Данных)
        protected override void ConfigureServices(IServiceCollection services, string localDbConnectionString)
        {
            services.AddSingleton(GetLocalDbSettings());
            services.AddStorageDependencies();
            services.AddCoreDependencies();
        }

        [Test]
        public async Task Create_One_Client_And_Check_List()
        {
            var api = Provider.GetService<IClientsApi>();

            var getClientsRequest = new GetClientsRequest
            {
                Paging = new PagingRequestModel(0, 1),
                Where = null,
                Sort = new[]
                {
                    new SortFieldModel
                    {
                        Type = SortEnum.Desc,
                        FieldName = "CreatedAt"
                    }
                }
            };
            
            var getClientsResponseBefore = await api.GetClientsAsync(getClientsRequest);

            getClientsResponseBefore.ShouldSatisfyAllConditions(
                () => getClientsResponseBefore.ShouldNotBeNull(),
                () => getClientsResponseBefore.Data.ShouldNotBeNull(),
                () => getClientsResponseBefore.Error.ShouldBeNull(),
                () => getClientsResponseBefore.Data.Total.ShouldBe(0));
            
            var createClientRequest = new CreateClientRequest
            {
                Name = "Иванов Иван"
            };

            var createClientResponse = await api.CreateClientAsync(createClientRequest);

            createClientRequest.ShouldSatisfyAllConditions(
                () => createClientResponse.ShouldNotBeNull(),
                () => createClientResponse.Data.ShouldNotBeNull(),
                () => createClientResponse.Error.ShouldBeNull(),
                () => createClientResponse.Data.Id.ShouldBeGreaterThan(0));
            
            var getClientsResponseAfter= await api.GetClientsAsync(getClientsRequest);

            getClientsResponseAfter.ShouldSatisfyAllConditions(
                () => getClientsResponseAfter.ShouldNotBeNull(),
                () => getClientsResponseAfter.Data.ShouldNotBeNull(),
                () => getClientsResponseAfter.Error.ShouldBeNull(),
                () => getClientsResponseAfter.Data.Items.ShouldNotBeNull(),
                () => getClientsResponseAfter.Data.Items.Count.ShouldBe(1),
                () => getClientsResponseAfter.Data.Items[0].Id.ShouldBeGreaterThan(0),
                () => getClientsResponseAfter.Data.Items[0].Name.ShouldBe(createClientRequest.Name),
                () => getClientsResponseAfter.Data.Total.ShouldBe(getClientsResponseBefore.Data.Total + 1));
        }
    }
}