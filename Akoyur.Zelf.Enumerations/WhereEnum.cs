﻿namespace Akoyur.Zelf.Enumerations
{
	/// <summary>
	/// Типы условных выражений
	/// </summary>
	public enum WhereEnum
	{
		/// <summary>
		/// Equal
		/// </summary>
		EQ = 1,

		/// <summary>
		/// Less than
		/// </summary>
		LT = 2,

		/// <summary>
		/// Less than or equal
		/// </summary>
		LTOQ = 3,

		/// <summary>
		/// Greater than
		/// </summary>
		GT = 4,

		/// <summary>
		/// Greater than or equal
		/// </summary>
		GTOQ = 5,

		/// <summary>
		/// Contains
		/// </summary>
		CT = 6,

		/// <summary>
		/// Any Equal
		/// </summary>
		AEQ = 7
	}
}
