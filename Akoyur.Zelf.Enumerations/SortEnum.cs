﻿namespace Akoyur.Zelf.Enumerations
{
	/// <summary>
	/// Тип сортировки
	/// </summary>
	public enum SortEnum
	{
		/// <summary>
		/// Ascending
		/// </summary>
		Asc = 1,

		/// <summary>
		/// Descending
		/// </summary>
		Desc = 2
	}
}
