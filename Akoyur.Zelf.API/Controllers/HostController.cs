﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Akoyur.Zelf.API.Models.v1.Requests.Base;
using Akoyur.Zelf.API.Models.v1.Responses.Base;
using Akoyur.Zelf.Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Akoyur.Zelf.API.Controllers
{
    /// <summary>
    /// Класс контроллера
    /// </summary>
    public class HostController : Controller
    {
        protected async Task<IActionResult> ExecuteAsync<TRequest, TResponse>(TRequest request, Func<TRequest, Task<BaseResponse<TResponse>>> methodAsync)
            where TResponse : class
            where TRequest : ValidatableRequestModel
        {
            if (request == null)
                return BadRequest(new BaseResponse<TResponse>(ValidationMessageHelper.GetRequestParametersNotDefined()));

            if (!request.IsValid(out var errors))
                return BadRequest(new BaseResponse<TResponse>(errors));

            var response = await methodAsync(request);

            if (response.Error != null)
                return BadRequest(new BaseResponse<TResponse>(response.Error));

            return Ok(response);
        }
        
        protected async Task<IActionResult> ExecuteAsync<TResponse>(Func<Task<BaseResponse<TResponse>>> methodAsync)
            where TResponse : class
        {
            var response = await methodAsync();

            if (response.Error != null)
                return BadRequest(new BaseResponse<TResponse>(response.Error));

            return Ok(response);
        }
    }
}