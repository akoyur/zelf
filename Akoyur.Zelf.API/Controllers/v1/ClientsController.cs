﻿using System;
using System.Net;
using System.Threading.Tasks;
using Akoyur.Zelf.API.Interfaces;
using Akoyur.Zelf.API.Models.v1.Requests.Clients;
using Akoyur.Zelf.API.Models.v1.Responses.Base;
using Akoyur.Zelf.API.Models.v1.Responses.Clients;
using Microsoft.AspNetCore.Mvc;

namespace Akoyur.Zelf.API.Controllers.v1
{
    /// <summary>
	/// Класс контроллера API "Клиенты"
	/// </summary>
	[ApiController]
	[Route("api/v1/clients")]
	public class ClientsController : HostController
	{
		private readonly IClientsApi _api;

		/// <summary>
		/// ctor
		/// </summary>
		public ClientsController(IClientsApi api) => _api = api ?? throw new ArgumentNullException(nameof(api));

		/// <summary>
		/// Зарегистрировать нового клиента
		/// </summary>
		[HttpPost("")]
		[ProducesResponseType((int) HttpStatusCode.OK, Type = typeof(BaseResponse<CreateClientResponse>))]
		[ProducesResponseType((int) HttpStatusCode.BadRequest, Type = typeof(string))]
		[ProducesResponseType((int) HttpStatusCode.NoContent, Type = typeof(string))]
		[ProducesResponseType((int) HttpStatusCode.InternalServerError, Type = typeof(string))]
		[ProducesResponseType((int) HttpStatusCode.GatewayTimeout, Type = typeof(string))]
		public async Task<IActionResult> CreateClientAsync(CreateClientRequest model)
		{
			return await ExecuteAsync(model, _api.CreateClientAsync);
		}

		/// <summary>
		/// Обновить клиента
		/// </summary>
		[HttpPut("{clientId}")]
		[ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(BaseResponse<UpdateClientResponse>))]
		[ProducesResponseType((int)HttpStatusCode.BadRequest, Type = typeof(string))]
		[ProducesResponseType((int)HttpStatusCode.NoContent, Type = typeof(string))]
		[ProducesResponseType((int)HttpStatusCode.InternalServerError, Type = typeof(string))]
		[ProducesResponseType((int)HttpStatusCode.GatewayTimeout, Type = typeof(string))]
		public async Task<IActionResult> UpdateClientAsync(int clientId, UpdateClientRequest model)
		{
			if (model != null)
			{
				model.Id = clientId;
			}

			return await ExecuteAsync(model, _api.UpdateClientAsync);
		}
		
		/// <summary>
		/// Получить информацию о клиенте
		/// </summary>
		[HttpGet("{clientId}")]
		[ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(BaseResponse<PagingResultModel<GetClientsResponse>>))]
		[ProducesResponseType((int)HttpStatusCode.BadRequest, Type = typeof(string))]
		[ProducesResponseType((int)HttpStatusCode.NoContent, Type = typeof(string))]
		[ProducesResponseType((int)HttpStatusCode.InternalServerError, Type = typeof(string))]
		[ProducesResponseType((int)HttpStatusCode.GatewayTimeout, Type = typeof(string))]
		public async Task<IActionResult> GetClientAsync(int clientId)
		{
			return await ExecuteAsync(() => _api.GetClientAsync(clientId));
		}

		/// <summary>
		/// Получить список клиентов
		/// </summary>
		[HttpPost("search")]
		[ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(BaseResponse<PagingResultModel<GetClientsResponse>>))]
		[ProducesResponseType((int)HttpStatusCode.BadRequest, Type = typeof(string))]
		[ProducesResponseType((int)HttpStatusCode.NoContent, Type = typeof(string))]
		[ProducesResponseType((int)HttpStatusCode.InternalServerError, Type = typeof(string))]
		[ProducesResponseType((int)HttpStatusCode.GatewayTimeout, Type = typeof(string))]
		public async Task<IActionResult> GetClientsAsync(GetClientsRequest model)
		{
			return await ExecuteAsync(model, _api.GetClientsAsync);
		}

		/// <summary>
		/// Удалить клиента
		/// </summary>
		[HttpDelete("{clientId}")]
		[ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(BaseResponse<DeleteClientResponse>))]
		[ProducesResponseType((int)HttpStatusCode.BadRequest, Type = typeof(string))]
		[ProducesResponseType((int)HttpStatusCode.NoContent, Type = typeof(string))]
		[ProducesResponseType((int)HttpStatusCode.InternalServerError, Type = typeof(string))]
		[ProducesResponseType((int)HttpStatusCode.GatewayTimeout, Type = typeof(string))]
		public async Task<IActionResult> Delete(int clientId)
		{
			return await ExecuteAsync(new DeleteClientRequest(clientId), _api.DeleteClientAsync);
		}

		/// <summary>
		/// Подписать одного клиента на другого
		/// </summary>
		[HttpPost("{clientId}/subscriptions")]
		[ProducesResponseType((int) HttpStatusCode.OK, Type = typeof(BaseResponse<SubscribeClientResponse>))]
		[ProducesResponseType((int) HttpStatusCode.BadRequest, Type = typeof(string))]
		[ProducesResponseType((int) HttpStatusCode.NoContent, Type = typeof(string))]
		[ProducesResponseType((int) HttpStatusCode.InternalServerError, Type = typeof(string))]
		[ProducesResponseType((int) HttpStatusCode.GatewayTimeout, Type = typeof(string))]
		public async Task<IActionResult> Subscribe(int clientId, SubscribeClientRequest model)
		{
			if (model != null)
			{
				model.ClientId = clientId;
			}

			return await ExecuteAsync(model, _api.SubscribeClientAsync);
		}

		/// <summary>
		/// Отписать одного клиента от другого
		/// </summary>
		[HttpDelete("{clientId}/subscriptions/{subscriptionId}")]
		[ProducesResponseType((int)HttpStatusCode.OK, Type = typeof(BaseResponse<UnSubscribeClientResponse>))]
		[ProducesResponseType((int)HttpStatusCode.BadRequest, Type = typeof(string))]
		[ProducesResponseType((int)HttpStatusCode.NoContent, Type = typeof(string))]
		[ProducesResponseType((int)HttpStatusCode.InternalServerError, Type = typeof(string))]
		[ProducesResponseType((int)HttpStatusCode.GatewayTimeout, Type = typeof(string))]
		public async Task<IActionResult> UnSubscribe(int clientId,  int subscriptionId)
		{
			return await ExecuteAsync(new UnSubscribeClientRequest(clientId,subscriptionId), _api.UnSubscribeClientAsync);
		}
	}
}